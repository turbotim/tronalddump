//
//  NetworkServiceMock.swift
//  TronaldDumpTests
//
//  Created by Tim Harrison on 07/01/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import UIKit
@testable import TronaldDump

enum MockService: String {
    case tags = "GetTags"
    case tag = "GetTag"
    case badJSON = "BadJSON"
}

class NetworkServiceMock: NetworkServiceProtocol {
    
    let mockService: MockService
    var calledURL: URL? = nil
    
    init(mockService: MockService) {
        self.mockService = mockService
    }
    
    func fetchData(fromURL url: URL, completion: @escaping (NetworkResponse) -> Void) {
        calledURL = url
        let path = Bundle(for: type(of: self)).path(forResource: mockService.rawValue, ofType: "json")!
        let data = try! Data(contentsOf: URL(fileURLWithPath: path))
        completion(.success(data))
    }
}
