//
//  TagsListViewModelTests.swift
//  TronaldDumpTests
//
//  Created by Tim Harrison on 07/01/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import XCTest
@testable import TronaldDump

class TagsListViewModelTests: XCTestCase {
    
    let tagsListViewModel = TagsListViewModel(apiService: APIService(networkService: NetworkServiceMock(mockService: .tags)))
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testTagListViewModelLoadsData() {
        let expect = XCTestExpectation(description: "Tag List Returned")
        tagsListViewModel.reloadTableViewClosure = {
            expect.fulfill()
            XCTAssertEqual(self.tagsListViewModel.numberOfCells(), 6)
        }
        tagsListViewModel.fetchTagList()
        wait(for: [expect], timeout: 1.0)
    }
    
    func testReturnsTagCellViewModel() {
        let expect = XCTestExpectation(description: "Tag List Returned")
        tagsListViewModel.reloadTableViewClosure = {
            expect.fulfill()
            let tagCellViewModel = self.tagsListViewModel.cellViewModel(atIndexPath: IndexPath(item: 0, section: 0))
            XCTAssertEqual(tagCellViewModel?.tagText, "Hillary Clinton")
        }
        tagsListViewModel.fetchTagList()
        wait(for: [expect], timeout: 1.0)
    }
}
