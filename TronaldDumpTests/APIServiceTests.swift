//
//  APIServiceTests.swift
//  TronaldDumpTests
//
//  Created by Tim Harrison on 07/01/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import XCTest
@testable import TronaldDump

class APIServiceTests: XCTestCase {
    
    var apiService: APIServiceProtocol? = nil
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        apiService = nil
        super.tearDown()
    }
    
    func testCanFetchCorrectNumberOfTags() {
        apiService = APIService(networkService: NetworkServiceMock(mockService: .tags))
        apiService?.fetchTags { (fetchTagsResponse) in
            switch fetchTagsResponse {
            case .failure(_):
                XCTFail()
            case .success(let tagsListResponse):
                XCTAssertEqual(tagsListResponse.tags.count, 6)
                XCTAssertEqual(tagsListResponse.count, 6)
            }
        }
    }
    
    func testCanFetchTagsContent() {
        apiService = APIService(networkService: NetworkServiceMock(mockService: .tags))
        apiService?.fetchTags { (fetchTagsResponse) in
            switch fetchTagsResponse {
            case .failure(_):
                XCTFail()
            case .success(let tagsListResponse):
                XCTAssertEqual(tagsListResponse.tags.first, "Hillary Clinton")
                XCTAssertEqual(tagsListResponse.tags.last, "Money")
            }
        }
    }
    
    func testCallsFetchTagsURL() {
        let networkServiceMock = NetworkServiceMock(mockService: .tags)
        let expectedURL = "https://api.tronalddump.io/tag"
        apiService = APIService(networkService: networkServiceMock)
        apiService?.fetchTags { (_) in
            let urlString = networkServiceMock.calledURL?.absoluteString
            XCTAssertEqual(urlString, expectedURL)
        }
    }
    
    func testFetchTagsReportsErrorWhenHandlingBadJSON() {
        apiService = APIService(networkService: NetworkServiceMock(mockService: .badJSON))
        apiService?.fetchTags { (fetchTagsResponse) in
            switch fetchTagsResponse {
            case .failure(let error):
               XCTAssertEqual(error, APIError.couldNotParseData)
            case .success(_):
                XCTFail()
            }
        }
    }
    
    func testCanFetchTag() {
        apiService = APIService(networkService: NetworkServiceMock(mockService: .tag))
        apiService?.fetchTag("TESTTAG") { (fetchTagResponse) in
            switch fetchTagResponse {
            case .failure(_):
                XCTFail()
            case .success(let tagResponse):
                XCTAssertEqual(tagResponse.count, 1)
            }
        }
    }
    
    func testFetchTagRequestReturnsQuotes() {
        apiService = APIService(networkService: NetworkServiceMock(mockService: .tag))
        apiService?.fetchTag("TESTTAG") { (fetchTagResponse) in
            switch fetchTagResponse {
            case .failure(_):
                XCTFail()
            case .success(let tagResponse):
                XCTAssertEqual(tagResponse.tagInfo.quotes.count, 1)
            }
        }
    }
    
    func testFetchTagRequestReturnsQuoteText() {
        let expectedQuoteText = "Why doesn't the media want to report that on the two \"Big Thursdays\" when Crooked Hillary and I made our speeches - Republican's won ratings"
        apiService = APIService(networkService: NetworkServiceMock(mockService: .tag))
        apiService?.fetchTag("TESTTAG") { (fetchTagResponse) in
            switch fetchTagResponse {
            case .failure(_):
                XCTFail()
            case .success(let tagResponse):
                XCTAssertEqual(tagResponse.tagInfo.quotes.first?.text, expectedQuoteText)
            }
        }
    }
    
    func testFetchTagRequestUsesTagNameInURL() {
        let networkServiceMock = NetworkServiceMock(mockService: .tag)
        let expectedURL = "https://api.tronalddump.io/tag/TESTTAG"
        apiService = APIService(networkService: networkServiceMock)
        apiService?.fetchTag("TESTTAG") { (_) in
            XCTAssertEqual(expectedURL, networkServiceMock.calledURL?.absoluteString)
        }
    }
    
    func testFetchTagReportsErrorWhenHandlingBadJSON() {
        apiService = APIService(networkService: NetworkServiceMock(mockService: .badJSON))
        apiService?.fetchTag("TESTTAG") { (fetchTagsResponse) in
            switch fetchTagsResponse {
            case .failure(let error):
                XCTAssertEqual(error, APIError.couldNotParseData)
            case .success(_):
                XCTFail()
            }
        }
    }
}
