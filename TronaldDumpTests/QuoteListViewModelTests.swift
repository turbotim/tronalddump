//
//  QuoteListViewModelTests.swift
//  TronaldDumpTests
//
//  Created by Tim Harrison on 07/01/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import XCTest
@testable import TronaldDump

class QuoteListViewModelTests: XCTestCase {
    
    let quoteListViewModel = QuoteListViewModel(tag: "TESTTAG", apiService: APIService(networkService: NetworkServiceMock(mockService: .tag)))
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testQuoteListViewModelLoadsData() {
        let expect = XCTestExpectation(description: "Quote List Returned")
        quoteListViewModel.reloadTableViewClosure = {
            expect.fulfill()
            XCTAssertEqual(self.quoteListViewModel.numberOfCells(), 1)
        }
        quoteListViewModel.fetchQuoteList()
        wait(for: [expect], timeout: 1.0)
    }
    
    func testReturnsQuoteCellViewModel() {
        let expect = XCTestExpectation(description: "Quote List Returned")
        let expectedQuoteText = "Why doesn't the media want to report that on the two \"Big Thursdays\" when Crooked Hillary and I made our speeches - Republican's won ratings"
        quoteListViewModel.reloadTableViewClosure = {
            expect.fulfill()
            let quoteCellViewModel = self.quoteListViewModel.cellViewModel(atIndexPath: IndexPath(item: 0, section: 0))
            XCTAssertEqual(quoteCellViewModel?.quoteText, expectedQuoteText)
        }
        quoteListViewModel.fetchQuoteList()
        wait(for: [expect], timeout: 1.0)
    }
}
