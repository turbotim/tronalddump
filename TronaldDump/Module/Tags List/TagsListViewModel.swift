//
//  TagsListViewModel.swift
//  TronaldDump
//
//  Created by Tim Harrison on 07/01/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import UIKit

class TagsListViewModel {

    var reloadTableViewClosure: (() -> Void)?
    var failureToLoadClosure: (() -> Void)?
    private let apiService: APIServiceProtocol
    private var tagList: TagsListResponse?
    
    init(apiService: APIServiceProtocol) {
        self.apiService = apiService
    }
    
    func fetchTagList() {
        apiService.fetchTags { [weak self] (fetchTagsResponse) in
            guard let strongSelf = self else {
                return
            }
            switch fetchTagsResponse {
            case .failure(_):
                strongSelf.failureToLoadClosure?()
            case .success(let tagListResponse):
                strongSelf.tagList = tagListResponse
                strongSelf.reloadTableViewClosure?()
            }
        }
    }
    
    func cellViewModel(atIndexPath indexPath: IndexPath) -> TagCellViewModel? {
        guard tagList?.tags.count ?? 0 > indexPath.item, let tagText = tagList?.tags[indexPath.item] else {
            return nil
        }
        return TagCellViewModel(tagText: tagText)
    }
    
    func numberOfCells() -> Int {
        return tagList?.count ?? 0
    }
}

struct TagCellViewModel {
    let tagText: String
}
