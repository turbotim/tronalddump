//
//  TagsListViewController.swift
//  TronaldDump
//
//  Created by Tim Harrison on 07/01/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import UIKit

class TagsListViewController: UITableViewController {

    private let tagListCellIdentifier = "TagListCellIdentifier"
    private lazy var viewModel: TagsListViewModel = {
        return TagsListViewModel(apiService: APIService(networkService: NetworkService()))
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Tag List"
        configureTableView()
        configureViewModel()
    }
    
    private func configureViewModel() {
        viewModel.reloadTableViewClosure = { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        viewModel.failureToLoadClosure = { [weak self] in
            let alert = UIAlertController(title: "Alert", message: "Failed To Load Data", preferredStyle: .alert)
            alert.addAction( UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            DispatchQueue.main.async {
                self?.present(alert, animated: true, completion: nil)
            }
        }
        viewModel.fetchTagList()
    }
    
    private func configureTableView() {
        tableView.register(TagListTableViewCell.self, forCellReuseIdentifier: tagListCellIdentifier)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
    }
}

//MARK: UITableViewDelegate
extension TagsListViewController {
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cellViewModel = viewModel.cellViewModel(atIndexPath: indexPath) else {
            return
        }
        let quoteListViewController = QuoteListViewController(tag: cellViewModel.tagText)
        navigationController?.pushViewController(quoteListViewController, animated: true)
    }
}

//MARK: UITableViewDataSource
extension TagsListViewController {
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfCells()
    }
    
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: tagListCellIdentifier, for: indexPath)
        guard let tagListCell = cell as? TagListTableViewCell,
            let cellViewModel = viewModel.cellViewModel(atIndexPath: indexPath) else {
            return cell
        }
        tagListCell.tagLabel.text = cellViewModel.tagText
        return tagListCell
    }
}
