//
//  QuoteListViewController.swift
//  TronaldDump
//
//  Created by Tim Harrison on 07/01/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import UIKit

class QuoteListViewController: UITableViewController {

    private let quoteListCellIdentifier = "QuoteListCellIdentifier"
    let quoteListViewModel: QuoteListViewModel
    
    init(tag: String) {
        self.quoteListViewModel = QuoteListViewModel(tag: tag, apiService: APIService(networkService: NetworkService()))
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Quotes"
        configureTableView()
        configureViewModel()
    }
    
    private func configureViewModel() {
        quoteListViewModel.reloadTableViewClosure = { [weak self] in
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        quoteListViewModel.failureToLoadClosure = { [weak self] in
            let alert = UIAlertController(title: "Alert", message: "Failed To Load Data", preferredStyle: .alert)
            alert.addAction( UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            DispatchQueue.main.async {
                self?.present(alert, animated: true, completion: nil)
            }
        }
        quoteListViewModel.fetchQuoteList()
    }
    
    private func configureTableView() {
        tableView.register(QuoteListTableViewCell.self, forCellReuseIdentifier: quoteListCellIdentifier)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 140
    }
}

//MARK: UITableViewDataSource
extension QuoteListViewController {
    
    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return quoteListViewModel.numberOfCells()
    }
    
    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: quoteListCellIdentifier, for: indexPath)
        guard let quoteCell = cell as? QuoteListTableViewCell,
            let cellViewModel = quoteListViewModel.cellViewModel(atIndexPath: indexPath) else {
                return cell
        }
        quoteCell.quoteLabel.text = cellViewModel.quoteText
        quoteCell.selectionStyle = .none
        return quoteCell
    }
}
