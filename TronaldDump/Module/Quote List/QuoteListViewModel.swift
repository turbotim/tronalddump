//
//  QuoteListViewModel.swift
//  TronaldDump
//
//  Created by Tim Harrison on 07/01/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import UIKit

class QuoteListViewModel {

    var reloadTableViewClosure: (() -> Void)?
    var failureToLoadClosure: (() -> Void)?
    private let apiService: APIServiceProtocol
    private let tag: String
    private var tagResponse: TagResponse?
    
    init(tag: String, apiService: APIServiceProtocol) {
        self.apiService = apiService
        self.tag = tag.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? tag
    }
    
    func fetchQuoteList() {
        apiService.fetchTag(tag) { [weak self] (fetchTagResponse) in
            guard let strongSelf = self else {
                return
            }
            switch fetchTagResponse {
            case .failure(_):
                strongSelf.failureToLoadClosure?()
            case .success(let tagResponse):
                strongSelf.tagResponse = tagResponse
                strongSelf.reloadTableViewClosure?()
            }
        }
    }
    
    func cellViewModel(atIndexPath indexPath: IndexPath) -> QuoteCellViewModel? {
        guard tagResponse?.tagInfo.quotes.count ?? 0 > indexPath.item, let quote = tagResponse?.tagInfo.quotes[indexPath.item] else {
            return nil
        }
        return QuoteCellViewModel(quoteText: quote.text)
    }
    
    func numberOfCells() -> Int {
        return tagResponse?.tagInfo.quotes.count ?? 0
    }
}

struct QuoteCellViewModel {
    let quoteText: String
}
