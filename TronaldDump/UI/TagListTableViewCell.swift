//
//  TagListTableViewCell.swift
//  TronaldDump
//
//  Created by Tim Harrison on 07/01/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import UIKit

class TagListTableViewCell: UITableViewCell {
    
    let tagLabel = UILabel(frame: .zero)
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureCell()
    }
    
    private func configureCell() {
        tagLabel.translatesAutoresizingMaskIntoConstraints = false
        tagLabel.textAlignment = .center
        addSubview(tagLabel)
        NSLayoutConstraint.activate([
            tagLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            tagLabel.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            tagLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            tagLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)
            ])
    }
}
