//
//  QuoteListTableViewCell.swift
//  TronaldDump
//
//  Created by Tim Harrison on 07/01/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import UIKit

class QuoteListTableViewCell: UITableViewCell {

    let quoteLabel = UILabel(frame: .zero)
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureCell()
    }
    
    private func configureCell() {
        quoteLabel.translatesAutoresizingMaskIntoConstraints = false
        quoteLabel.textAlignment = .center
        quoteLabel.numberOfLines = 0
        addSubview(quoteLabel)
        NSLayoutConstraint.activate([
            quoteLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 10),
            quoteLabel.topAnchor.constraint(equalTo: topAnchor, constant: 10),
            quoteLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -10),
            quoteLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10)
            ])
    }
}
