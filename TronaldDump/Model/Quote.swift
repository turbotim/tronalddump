//
//  Quote.swift
//  TronaldDump
//
//  Created by Tim Harrison on 07/01/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import UIKit

struct Quote: Codable {
    
    let text: String
    
    private enum CodingKeys: String, CodingKey {
        case text = "value"
    }
}
