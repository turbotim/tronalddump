//
//  TagResponse.swift
//  TronaldDump
//
//  Created by Tim Harrison on 07/01/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import Foundation

struct TagsListResponse: Codable {
    let count: Int
    let tags: [String]
    
    private enum CodingKeys: String, CodingKey {
        case tags = "_embedded"
        case count
    }
}
