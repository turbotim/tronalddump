//
//  TagResponse.swift
//  TronaldDump
//
//  Created by Tim Harrison on 07/01/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import Foundation

struct TagInfo: Codable {
    let quotes: [Quote]
    
    private enum CodingKeys: String, CodingKey {
        case quotes = "tags"
    }
}

struct TagResponse: Codable {
    let count: Int
    let total: Int
    let tagInfo: TagInfo
    
    private enum CodingKeys: String, CodingKey {
        case count
        case total
        case tagInfo = "_embedded"
    }
}
