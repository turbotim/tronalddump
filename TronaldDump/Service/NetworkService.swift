//
//  NetworkService.swift
//  TronaldDump
//
//  Created by Tim Harrison on 07/01/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import Foundation

enum NetworkError: Error {
    case networkingFailed
}

enum NetworkResponse {
    case success(Data)
    case failure(NetworkError)
}

protocol NetworkServiceProtocol {
    func fetchData(fromURL url: URL, completion: @escaping (NetworkResponse) -> Void)
}

class NetworkService: NetworkServiceProtocol {
    
    private let session = URLSession.shared
    
    func fetchData(fromURL url: URL, completion: @escaping (NetworkResponse) -> Void) {
        let dataTask = session.dataTask(with: url) { (data, response, error) in
            guard let data = data else {
                completion(.failure(.networkingFailed))
                return
            }
            completion(.success(data))
        }
        dataTask.resume()
    }
}
