//
//  APIService.swift
//  TronaldDump
//
//  Created by Tim Harrison on 07/01/2018.
//  Copyright © 2018 Tim Harrison. All rights reserved.
//

import Foundation

enum APIError {
    case couldNotParseData
    case couldNotBuildURL
    case networkFailure
}

enum FetchTagsListResponse {
    case failure(APIError)
    case success(TagsListResponse)
}

enum FetchTagResponse {
    case failure(APIError)
    case success(TagResponse)
}

protocol APIServiceProtocol {
    func fetchTags(completion: @escaping (FetchTagsListResponse) -> Void)
    func fetchTag(_ tag: String, completion: @escaping (FetchTagResponse) -> Void)
}

class APIService: APIServiceProtocol {
    
    private let networkService: NetworkServiceProtocol
    private let tagAPIBaseURL = "https://api.tronalddump.io/tag"
    
    init(networkService: NetworkServiceProtocol) {
        self.networkService = networkService
    }
    
    func fetchTags(completion: @escaping (FetchTagsListResponse) -> Void) {
        
        guard let fetchTagsURL = URL(string: tagAPIBaseURL) else {
            completion(.failure(.couldNotBuildURL))
            return
        }
        
        networkService.fetchData(fromURL: fetchTagsURL) { (networkResponse) in
            
            let data: Data
            switch networkResponse {
            case .failure(_):
                completion(.failure(.networkFailure))
                return
            case .success(let networkData):
                data = networkData
            }
            
            do {
                let decoder = JSONDecoder()
                let tagsListResponse = try decoder.decode(TagsListResponse.self, from: data)
                completion(.success(tagsListResponse))
            } catch {
                completion(.failure(.couldNotParseData))
            }
        }
    }
    
    func fetchTag(_ tag: String, completion: @escaping (FetchTagResponse) -> Void) {
        guard let fetchTagURL = URL(string: "\(tagAPIBaseURL)/\(tag)") else {
            completion(.failure(.couldNotBuildURL))
            return
        }
        
        networkService.fetchData(fromURL: fetchTagURL) { (networkResponse) in
            
            let data: Data
            switch networkResponse {
            case .failure(_):
                completion(.failure(.networkFailure))
                return
            case .success(let networkData):
                data = networkData
            }

            do {
                let decoder = JSONDecoder()
                let tagResponse = try decoder.decode(TagResponse.self, from: data)
                completion(.success(tagResponse))
            } catch {
                completion(.failure(.couldNotParseData))
            }
        }
    }
}
